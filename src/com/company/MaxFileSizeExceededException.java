package com.company;

public class MaxFileSizeExceededException extends  Exception {
    MaxFileSizeExceededException(String ex){
        super(ex);
    }
}

package com.company;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class JaxbMarshaller {

    public static <T> void marshal(String rootName, Collection<T> c, File f) throws JAXBException
    {
        // Create context with generic type
        JAXBContext ctx = JAXBContext.newInstance(findTypes(c));
        Marshaller m = ctx.createMarshaller();

        // Create wrapper collection
        JAXBElement element = createCollectionElement(rootName, c);
        m.marshal(element, f);
    }
    protected static <T> JAXBElement createCollectionElement(String rootName, Collection<T> c)
    {
        JAXBCollection collection = new JAXBCollection(c);
        return new JAXBElement<JAXBCollection>(new QName(rootName), JAXBCollection.class, collection);
    }

    protected static <T> Class[] findTypes(Collection<T> c)
    {
        Set<Class> types = new HashSet<Class>();
        types.add(JAXBCollection.class);
        for (T o : c)
        {
            if (o != null)
            {
                types.add(o.getClass());
            }
        }
        return types.toArray(new Class[0]);
    }

}
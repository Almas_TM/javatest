package com.company;

import javax.swing.filechooser.FileSystemView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

public class TxtFileResults extends Results { //наследование
    private static final int ONE_KB = 1024;
    private static TxtFileResults single_instance=null;
    public String path;
    private TxtFileResults(){
        path=FileSystemView.getFileSystemView().getHomeDirectory()+"\\JavaTest.txt";
    }
    public static TxtFileResults getInstance(){
        if(single_instance==null)
            single_instance= new TxtFileResults();
        return single_instance;
    }
    @Override//polymorphism run time
    public void SaveResults(GenArray genArray) {
        File file = new File(path);
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
            Object[] objArray = genArray.getObjArray();
            for (int i = 0; i < objArray.length; i++) {
                String data = objArray[i].toString() + "\n";
                int dataLength = data.getBytes("UTF-8").length;
                if(file.length() + dataLength > ONE_KB)
                    throw  new MaxFileSizeExceededException(String.valueOf(ONE_KB) + " Max txt file size exceeded");
                writer.write(objArray[i].toString() + "\n");
                writer.flush();
            }
        } catch (IOException | MaxFileSizeExceededException e) {
            file.delete();
            e.printStackTrace();
        }
    }
}

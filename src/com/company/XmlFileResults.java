package com.company;

import javax.swing.filechooser.FileSystemView;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XmlFileResults extends Results {//наследование
    private static final int ONE_KB = 2048;
    @Override//polymorphism run time
    public void SaveResults(GenArray genArray) {
        String path=FileSystemView.getFileSystemView().getHomeDirectory()+"\\XMLResults.xml";
        File file = new File(path);
        try{
            List<Object> ss = Arrays.asList(genArray.getObjArray());
            int len = genArray.getObjArray().length;
            JaxbMarshaller.marshal("Squares", ss, file);
            if(file.length()  > ONE_KB){
                throw  new MaxFileSizeExceededException(String.valueOf(ONE_KB) + " Max xml file size exceeded");
            }
        }
        catch (JAXBException | MaxFileSizeExceededException ex){
            file.delete();
            ex.printStackTrace();
        }
    }
}

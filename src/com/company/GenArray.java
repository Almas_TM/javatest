package com.company;

import java.lang.reflect.Array;

public class GenArray<T> {
    private T[] objArray;

    public T[] getObjArray() {//инкапсуляции
        return objArray;
    }
    public void setItem (int i, T t) {
        objArray[i] = t;
    }//инкапсуляции

    public GenArray(Class<T[]> clazz, int length) {
        objArray = clazz.cast(Array.newInstance(clazz.getComponentType(), length));
    }
}

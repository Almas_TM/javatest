package com.company;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Square{
    private int number;
    private boolean isEven;

    public Square() {
    }
    public Square(int number, boolean isEven) {
        this.number=number;
        this.isEven=isEven;
    }
    //инкапсуляции
    public boolean getIsEven() {
        return isEven;
    }
    @XmlElement
    public void setIsEven(boolean even) {
        isEven = even;
    }//инкапсуляции
    @XmlElement
    public void setNumber(int val) {
        this.number = val;
    }//инкапсуляции
    public int getNumber() {
        return number;
    }//инкапсуляции
    public String toString(){//overriding the toString() method
        return number +", number is even: "+isEven;
    }
}

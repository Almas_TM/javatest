package com.company;

import javax.xml.bind.annotation.XmlAnyElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JAXBCollection<T>
{
    @XmlAnyElement(lax = true)
    private ArrayList<T> items;

    public JAXBCollection(Collection<T> contents)
    {
            items = new ArrayList<T>(contents);
    }
}
package com.company;


public class Main {

    public static void main(String[] args) {

        if(args!=null && args.length>0) {
            try {

                int limit = Integer.parseInt(args[0]);
                int[] intArray = GetTestArray(100);

                int innerLimit = limit>intArray.length? intArray.length:limit;
                GenArray<Square> genArray = new GenArray<Square>(Square[].class, innerLimit);

                for (int i = 0; i < innerLimit; i++) {
                    int sqr = intArray[i]*intArray[i];
                    //System.out.println(sqr);//first task changed by second task
                    Square sqrObject = new Square(sqr, sqr%2==0?true:false);
                    genArray.setItem(i,sqrObject);
                    System.out.println(sqrObject.getIsEven() + "" + sqrObject.getNumber());
                }
                Results txtResults = TxtFileResults.getInstance();//polymorphism
                txtResults.SaveResults(genArray);
                Results xmlFileResults = new XmlFileResults();//polymorphism
                xmlFileResults.SaveResults(genArray);
            }
            catch (Exception ex){
                System.out.println("Некорректная тип параметра, вместо этого значения ожидаются целые числа - "+ args[ 0 ]);
            }
        }
        else
            System.out.println("Верхний предел не передан как параметр");
    }

    private static int[] GetTestArray(int arrSize) {
        int[] intArray = new int[arrSize];
        for (int i = 0; i < arrSize; i++)
            intArray[i]=i;
        return  intArray;
    }


}
